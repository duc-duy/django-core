from django.urls import path
from .views import PostList, PostSearchView, PostDetail

urlpatterns = [
    path('posts', PostList.as_view(), name='post-list'),
    path('posts/<int:pk>', PostDetail.as_view(), name='post-detail'),
    path('posts/search', PostSearchView.as_view(), name='post-search'),
]