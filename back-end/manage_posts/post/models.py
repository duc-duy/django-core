from django.db import models
from category.models import Category

# Create your models here.
class Post(models.Model):
    PostId = models.AutoField(primary_key=True, db_column='post_id')
    Title = models.CharField(max_length=200, db_column='title')
    Content = models.TextField( db_column='content')
    AmountView = models.IntegerField(db_column='amount_view', default=0)
    SubmitDate = models.DateField(db_column='submit_date')
    UpdateLastDate = models.DateField(db_column='update_last_date')
    AccountId = models.IntegerField(db_column='account_id')
    CategoryId = models.ForeignKey(Category, on_delete=models.CASCADE)

