from rest_framework import serializers
from .models import Post

class PostSerializers(serializers.ModelSerializer):
    class Meta:
        db_table = 'posts'
        model = Post
        fields = ['PostId','Title','Content','AmountView','SubmitDate','UpdateLastDate','AccountId','CategoryId']
        managed = True
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'

# class PostCreateSerializers(serializers.ModelSerializer):
#     class Meta:
#         db_table = 'posts'
#         model = Post
#         fields = ['PostId','Title','Content','AccountId','CategoryId']
#         managed = True
#         verbose_name = 'Post'
#         verbose_name_plural = 'Posts'
# class PostUpdateSerializers(serializers.ModelSerializer):
#     class Meta:
#         db_table = 'posts'
#         model = Post
#         fields = ['PostId','Title','Content','UpdateLastDate','CategoryId']
#         managed = True
#         verbose_name = 'Post'
#         verbose_name_plural = 'Posts'
