from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Category
from .serializers import CategorySerializer

# Create your views here.
class CategoryList(APIView):
    serializer_class = CategorySerializer

    def get(self, request, format=None):
        data = Category.objects.all()
        serializer = CategorySerializer(data, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = CategorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CategorySearchView(APIView):
    serializer_class = CategorySerializer
    
    def get(self, request):
        query = request.query_params.get('q', '')
        if query == '':
            data = Category.objects.all()
        else:
            data = Category.objects.filter(CategoryName__icontains=query)
        serializer = CategorySerializer(data, many=True)
        return Response(serializer.data)


class CategoryDetail(APIView):
    serializer_class = CategorySerializer

    def get_object(self, pk):
        return get_object_or_404(Category, pk=pk)

    def get(self, request, pk, format=None):
        data = self.get_object(pk)
        serializer = CategorySerializer(data)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        data = self.get_object(pk)
        serializer = CategorySerializer(data, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        data = self.get_object(pk)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
