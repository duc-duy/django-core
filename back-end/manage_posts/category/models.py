from django.db import models

# Create your models here.
class Category(models.Model):
    CategoryId = models.AutoField(primary_key=True, db_column="category_id")
    CategoryName = models.CharField(max_length=100, db_column="category_name")
    CategoryParentId = models.IntegerField(db_column="category_parent_id")