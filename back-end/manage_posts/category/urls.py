from django.urls import path
from .views import CategoryList, CategoryDetail, CategorySearchView

urlpatterns = [
    path('categories', CategoryList.as_view(), name='categories-list'),
    path('categories/<int:pk>', CategoryDetail.as_view(), name='company-detail'),
    path('categories/search', CategorySearchView.as_view(), name='company-search'),
]