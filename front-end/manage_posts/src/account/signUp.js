import React, { useState } from 'react';
// import SERVER_URL from '../api/AxiosConnect';
// import { notify_success, notify_fail } from "../common/Notification";

const SignUp = () => {
    const [userName, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [verifyPassword, setVerifyPassword] = useState('');
    const [name, setName] = useState('');
    const [dateOfBirth, setDateOfBirth] = useState('');
    const [numberPhone, setNumberPhone] = useState('');
    const [email, setEmail] = useState('');

    const handleSubmit = (event) => {
        event.preventDefault();
        // SERVER_URL.post(`/postAccount`, {
        //     username: userName,
        //     password: password,
        //     name: name,
        //     dateOfBirth: dateOfBirth,
        //     numberPhone: numberPhone,
        //     email: email,
        // })
        //     .then((res) => {
        //         if (res.data.status === 200) {
        //             notify_success("sign-up");
        //         }
        //         else {
        //             notify_fail("sign-up");
        //         }
        //     })
        //     .catch(error => {
        //         notify_fail("sign-up");
        //     });
    }

    return (
        <form onSubmit={handleSubmit}>
            <table>
                <tr>
                    <th>
                        <h2>Sign-Out</h2>
                    </th>
                </tr>
                <tr>

                </tr>
                <tr>
                    <td>
                        <input
                            type="text"
                            value={userName}
                            onChange={(event) => setUsername(event.target.value)}
                            placeholder='username enter'
                        />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input
                            type="password"
                            value={password}
                            onChange={(event) => setPassword(event.target.value)}
                            placeholder='password enter'
                        />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input
                            type="password"
                            value={verifyPassword}
                            onChange={(event) => setVerifyPassword(event.target.value)}
                            placeholder='Verify password enter'
                        />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input
                            type="text"
                            value={name}
                            onChange={(event) => setName(event.target.value)}
                            placeholder='Name enter'
                        />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input
                            style={{ width: '95%' }}
                            type="date"
                            value={dateOfBirth}
                            onChange={(event) => setDateOfBirth(event.target.value)}
                            placeholder='Date of birth enter'
                        />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input
                            type="text"
                            value={numberPhone}
                            onChange={(event) => setNumberPhone(event.target.value)}
                            placeholder='Number phone enter'
                        />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input
                            type="email"
                            value={email}
                            onChange={(event) => setEmail(event.target.value)}
                            placeholder='Email enter'
                        />
                    </td>
                </tr>
                <tr style={{ textAlign: 'right' }}>
                    <td>
                        <button type="submit">Sign-up</button>
                    </td>
                </tr>
            </table>
        </form>
    );
};

export default SignUp;
