import React, { useEffect, useState } from 'react';
import SERVER_URL from '../../utils/api/AxiosConnect';

function ListUser() {

    const [users, setUsers] = useState(null)

    useEffect(() => {
        SERVER_URL.get('/getQuestions')
            .then((res) => {
                if (res.data.status === 200) {
                    setUsers(res.data);
                }
                else {
                    console.log("res.data: null");
                }
            })
            .catch(error => {
                console.log("res.data: error");
            });
    }, []);

    if (!users) {
        return <div>Loading...</div>;
    }

    return (
        <>
            <div style={{ textAlign: 'left' }}><a href="/dashboard">Dashboard</a></div><br />
            <table border='1'>
                <thead>
                    <tr>
                        <th>username</th>
                        <th>nameUser</th>
                        <th>dateOfBirth</th>
                        <th>email</th>
                        <th>dateCreate</th>
                        <th>dateUpdate</th>
                        <th colSpan='2'>action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>user1</td>
                        <td>member 1</td>
                        <td>02/12/1998</td>
                        <td>user1@gmail.com</td>
                        <td>11/04/2023</td>
                        <td>11/04/2023</td>
                        <td><a href="/update-user?id=1">Edit</a></td>
                        <td><a href="/delete-user">Delete</a></td>
                    </tr>
                    <tr>
                        <td>user2</td>
                        <td>member 2</td>
                        <td>07/02/2000</td>
                        <td>user2@gmail.com</td>
                        <td>11/04/2023</td>
                        <td>11/04/2023</td>
                        <td><a href="/update-user?id=2">Edit</a></td>
                        <td><a href="/delete-user">Delete</a></td>
                    </tr>
                    <tr>
                        <td>user3</td>
                        <td>member 3</td>
                        <td>15/10/1999</td>
                        <td>user3@gmail.com</td>
                        <td>11/04/2023</td>
                        <td>11/04/2023</td>
                        <td><a href="/update-user?id=3">Edit</a></td>
                        <td><a href="/delete-user">Delete</a></td>
                    </tr>
                </tbody>
            </table>
        </>
    );
}

export default ListUser;