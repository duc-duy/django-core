import SERVER_URL from '../api/AxiosConnect'
import { notify_success, notify_fail } from '../common/Notification';

export const METHOD_GET = async (url) => {
    try {
        const response = await SERVER_URL.get(url);
        return response.data;
      } catch (error) {
        console.log(error);
        return null;
      }
  };

// export const METHOD_GET = (url) => {
//     SERVER_URL.get(url)
//         .then((res) => {
//             if (res.data.status === 200) {
//                 return res.data;
//             }
//             else {
//                 console.log("res.data: null");
//             }
//         })
//         .catch(error => {
//             console.log("res.data: error");
//         });
// }

export const METHOD_POST = (props) => {
    const {url, message, param} = props
    SERVER_URL.post(url, param)
        .then((res) => {
            if (res.data.status === 200) {
                notify_success(message);
            }
            else {
                notify_fail(message);
            }
        })
        .catch(error => {
            notify_fail(message);
        });
}